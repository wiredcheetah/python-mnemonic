python-mnemonic
===============

.. image:: https://travis-ci.org/trezor/python-mnemonic.svg?branch=master
    :target: https://travis-ci.org/trezor/python-mnemonic

Reference implementation of BIP-0039: Mnemonic code for generating
deterministic keys

Dependency for /mnemonic/mnemonic.py: pbkdf2, python-pip, python

for ubuntu and similar:

sudo apt update; sudo apt install -y python python-pip pbkdf2

Usage:

python mnemonic.py

Usage to backup hsm_secret from c-lightning:

sudo apt update; sudo apt install -y basez git

sudo apt update; sudo apt install -y python python-pip pbkdf2

cd ~

git clone https://gitlab.com/wiredcheetah/python-mnemonic; cd python-mnemonic

verify that it is the same as https://github.com/trezor/python-mnemonic/commit/493e324df9b70e3bb195984fe6e86e01a477c777, other than this README,  and that it is not malicious

cat ~/.lightning/hsm_secret | hex | python ./mnemonic/mnemonic.py

Abstract
--------

This BIP describes the implementation of a mnemonic code or mnemonic sentence --
a group of easy to remember words -- for the generation of deterministic wallets.

It consists of two parts: generating the mnenomic, and converting it into a
binary seed. This seed can be later used to generate deterministic wallets using
BIP-0032 or similar methods.

BIP Paper
---------

See https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki
for full specification
